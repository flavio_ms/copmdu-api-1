﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace COPMDU.Infrastructure.Migrations
{
    public partial class CityOperator : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "OperatorId",
                table: "City",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 22,
                column: "OperatorId",
                value: 220);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 66,
                column: "OperatorId",
                value: 66);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 121,
                column: "OperatorId",
                value: 121);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 178,
                column: "OperatorId",
                value: 178);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 194,
                column: "OperatorId",
                value: 194);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 233,
                column: "OperatorId",
                value: 23);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 272,
                column: "OperatorId",
                value: 2);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 393,
                column: "OperatorId",
                value: 393);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 540,
                column: "OperatorId",
                value: 540);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 696,
                column: "OperatorId",
                value: 96);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 863,
                column: "OperatorId",
                value: 63);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 1097,
                column: "OperatorId",
                value: 97);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 1521,
                column: "OperatorId",
                value: 521);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 1637,
                column: "OperatorId",
                value: 637);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 1695,
                column: "OperatorId",
                value: 95);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 1833,
                column: "OperatorId",
                value: 133);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 1847,
                column: "OperatorId",
                value: 847);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 1907,
                column: "OperatorId",
                value: 907);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 2152,
                column: "OperatorId",
                value: 152);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 2235,
                column: "OperatorId",
                value: 223);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 2269,
                column: "OperatorId",
                value: 269);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 2291,
                column: "OperatorId",
                value: 291);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 2480,
                column: "OperatorId",
                value: 480);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 2550,
                column: "OperatorId",
                value: 550);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 2750,
                column: "OperatorId",
                value: 750);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 3042,
                column: "OperatorId",
                value: 42);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 3230,
                column: "OperatorId",
                value: 230);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 3363,
                column: "OperatorId",
                value: 336);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 3455,
                column: "OperatorId",
                value: 455);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 3514,
                column: "OperatorId",
                value: 514);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 3741,
                column: "OperatorId",
                value: 741);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 3753,
                column: "OperatorId",
                value: 753);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 3778,
                column: "OperatorId",
                value: 778);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 3832,
                column: "OperatorId",
                value: 832);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 3930,
                column: "OperatorId",
                value: 930);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4005,
                column: "OperatorId",
                value: 405);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4024,
                column: "OperatorId",
                value: 402);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4060,
                column: "OperatorId",
                value: 60);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4126,
                column: "OperatorId",
                value: 126);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4275,
                column: "OperatorId",
                value: 275);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4312,
                column: "OperatorId",
                value: 312);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4437,
                column: "OperatorId",
                value: 37);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4459,
                column: "OperatorId",
                value: 459);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4541,
                column: "OperatorId",
                value: 541);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4742,
                column: "OperatorId",
                value: 742);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4763,
                column: "OperatorId",
                value: 763);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4770,
                column: "OperatorId",
                value: 477);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4780,
                column: "OperatorId",
                value: 780);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4803,
                column: "OperatorId",
                value: 803);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4806,
                column: "OperatorId",
                value: 806);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4821,
                column: "OperatorId",
                value: 821);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4834,
                column: "OperatorId",
                value: 834);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4911,
                column: "OperatorId",
                value: 911);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 4917,
                column: "OperatorId",
                value: 917);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5066,
                column: "OperatorId",
                value: 566);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5078,
                column: "OperatorId",
                value: 507);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5083,
                column: "OperatorId",
                value: 508);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5100,
                column: "OperatorId",
                value: 100);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5105,
                column: "OperatorId",
                value: 105);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5115,
                column: "OperatorId",
                value: 115);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5116,
                column: "OperatorId",
                value: 116);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5129,
                column: "OperatorId",
                value: 529);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5161,
                column: "OperatorId",
                value: 161);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5197,
                column: "OperatorId",
                value: 197);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5226,
                column: "OperatorId",
                value: 226);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5228,
                column: "OperatorId",
                value: 228);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5230,
                column: "OperatorId",
                value: 30);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5237,
                column: "OperatorId",
                value: 237);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5239,
                column: "OperatorId",
                value: 239);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5250,
                column: "OperatorId",
                value: 250);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5268,
                column: "OperatorId",
                value: 268);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5285,
                column: "OperatorId",
                value: 285);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5305,
                column: "OperatorId",
                value: 305);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5316,
                column: "OperatorId",
                value: 316);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5320,
                column: "OperatorId",
                value: 320);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5338,
                column: "OperatorId",
                value: 338);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5359,
                column: "OperatorId",
                value: 359);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5386,
                column: "OperatorId",
                value: 386);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5389,
                column: "OperatorId",
                value: 538);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5399,
                column: "OperatorId",
                value: 399);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5404,
                column: "OperatorId",
                value: 404);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5409,
                column: "OperatorId",
                value: 409);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5413,
                column: "OperatorId",
                value: 413);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5419,
                column: "OperatorId",
                value: 419);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5420,
                column: "OperatorId",
                value: 420);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5423,
                column: "OperatorId",
                value: 423);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5442,
                column: "OperatorId",
                value: 442);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5443,
                column: "OperatorId",
                value: 443);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5448,
                column: "OperatorId",
                value: 448);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5449,
                column: "OperatorId",
                value: 8);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5456,
                column: "OperatorId",
                value: 456);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5464,
                column: "OperatorId",
                value: 464);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5471,
                column: "OperatorId",
                value: 471);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5474,
                column: "OperatorId",
                value: 474);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5487,
                column: "OperatorId",
                value: 487);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5488,
                column: "OperatorId",
                value: 488);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5498,
                column: "OperatorId",
                value: 498);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5509,
                column: "OperatorId",
                value: 52);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5530,
                column: "OperatorId",
                value: 530);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5531,
                column: "OperatorId",
                value: 553);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5533,
                column: "OperatorId",
                value: 533);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5543,
                column: "OperatorId",
                value: 543);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5565,
                column: "OperatorId",
                value: 565);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5567,
                column: "OperatorId",
                value: 567);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5569,
                column: "OperatorId",
                value: 569);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5572,
                column: "OperatorId",
                value: 572);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5573,
                column: "OperatorId",
                value: 573);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5576,
                column: "OperatorId",
                value: 576);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5577,
                column: "OperatorId",
                value: 577);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5586,
                column: "OperatorId",
                value: 586);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5595,
                column: "OperatorId",
                value: 595);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5599,
                column: "OperatorId",
                value: 599);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5623,
                column: "OperatorId",
                value: 56);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5656,
                column: "OperatorId",
                value: 656);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5659,
                column: "OperatorId",
                value: 659);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5661,
                column: "OperatorId",
                value: 661);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5668,
                column: "OperatorId",
                value: 668);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5693,
                column: "OperatorId",
                value: 54);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5712,
                column: "OperatorId",
                value: 712);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5714,
                column: "OperatorId",
                value: 714);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5715,
                column: "OperatorId",
                value: 715);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5722,
                column: "OperatorId",
                value: 722);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5739,
                column: "OperatorId",
                value: 739);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5746,
                column: "OperatorId",
                value: 746);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5749,
                column: "OperatorId",
                value: 574);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5756,
                column: "OperatorId",
                value: 756);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5761,
                column: "OperatorId",
                value: 761);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5765,
                column: "OperatorId",
                value: 765);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5769,
                column: "OperatorId",
                value: 769);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5776,
                column: "OperatorId",
                value: 55);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5791,
                column: "OperatorId",
                value: 791);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5795,
                column: "OperatorId",
                value: 795);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5798,
                column: "OperatorId",
                value: 798);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5810,
                column: "OperatorId",
                value: 810);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5820,
                column: "OperatorId",
                value: 820);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5833,
                column: "OperatorId",
                value: 833);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5850,
                column: "OperatorId",
                value: 850);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5856,
                column: "OperatorId",
                value: 856);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5863,
                column: "OperatorId",
                value: 863);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5865,
                column: "OperatorId",
                value: 865);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5869,
                column: "OperatorId",
                value: 869);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5882,
                column: "OperatorId",
                value: 882);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5910,
                column: "OperatorId",
                value: 910);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5923,
                column: "OperatorId",
                value: 592);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5924,
                column: "OperatorId",
                value: 924);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5954,
                column: "OperatorId",
                value: 954);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5966,
                column: "OperatorId",
                value: 966);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5972,
                column: "OperatorId",
                value: 972);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5976,
                column: "OperatorId",
                value: 976);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 5985,
                column: "OperatorId",
                value: 9);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6009,
                column: "OperatorId",
                value: 609);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6022,
                column: "OperatorId",
                value: 22);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6024,
                column: "OperatorId",
                value: 24);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6027,
                column: "OperatorId",
                value: 27);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6036,
                column: "OperatorId",
                value: 36);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6048,
                column: "OperatorId",
                value: 48);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6067,
                column: "OperatorId",
                value: 67);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6070,
                column: "OperatorId",
                value: 5);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6076,
                column: "OperatorId",
                value: 76);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6095,
                column: "OperatorId",
                value: 295);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6103,
                column: "OperatorId",
                value: 103);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6108,
                column: "OperatorId",
                value: 108);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6121,
                column: "OperatorId",
                value: 621);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6129,
                column: "OperatorId",
                value: 129);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6139,
                column: "OperatorId",
                value: 4);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6141,
                column: "OperatorId",
                value: 141);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6143,
                column: "OperatorId",
                value: 143);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6144,
                column: "OperatorId",
                value: 53);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6154,
                column: "OperatorId",
                value: 615);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6158,
                column: "OperatorId",
                value: 6);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6162,
                column: "OperatorId",
                value: 162);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6178,
                column: "OperatorId",
                value: 678);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6182,
                column: "OperatorId",
                value: 182);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6187,
                column: "OperatorId",
                value: 187);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6195,
                column: "OperatorId",
                value: 195);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6201,
                column: "OperatorId",
                value: 7);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6207,
                column: "OperatorId",
                value: 207);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6209,
                column: "OperatorId",
                value: 209);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6216,
                column: "OperatorId",
                value: 216);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6233,
                column: "OperatorId",
                value: 233);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6235,
                column: "OperatorId",
                value: 235);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6241,
                column: "OperatorId",
                value: 241);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6246,
                column: "OperatorId",
                value: 246);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6261,
                column: "OperatorId",
                value: 261);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6272,
                column: "OperatorId",
                value: 272);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6276,
                column: "OperatorId",
                value: 276);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6277,
                column: "OperatorId",
                value: 277);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6279,
                column: "OperatorId",
                value: 279);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6282,
                column: "OperatorId",
                value: 282);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6294,
                column: "OperatorId",
                value: 294);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6322,
                column: "OperatorId",
                value: 322);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6333,
                column: "OperatorId",
                value: 333);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6379,
                column: "OperatorId",
                value: 379);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6389,
                column: "OperatorId",
                value: 389);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6413,
                column: "OperatorId",
                value: 641);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6430,
                column: "OperatorId",
                value: 430);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6437,
                column: "OperatorId",
                value: 437);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6531,
                column: "OperatorId",
                value: 531);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6749,
                column: "OperatorId",
                value: 749);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6770,
                column: "OperatorId",
                value: 770);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6794,
                column: "OperatorId",
                value: 794);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6851,
                column: "OperatorId",
                value: 851);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 6923,
                column: "OperatorId",
                value: 923);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7052,
                column: "OperatorId",
                value: 752);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7058,
                column: "OperatorId",
                value: 58);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7070,
                column: "OperatorId",
                value: 70);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7163,
                column: "OperatorId",
                value: 163);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7193,
                column: "OperatorId",
                value: 193);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7194,
                column: "OperatorId",
                value: 94);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7203,
                column: "OperatorId",
                value: 203);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7213,
                column: "OperatorId",
                value: 213);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7260,
                column: "OperatorId",
                value: 260);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7278,
                column: "OperatorId",
                value: 278);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7363,
                column: "OperatorId",
                value: 363);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7458,
                column: "OperatorId",
                value: 458);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7560,
                column: "OperatorId",
                value: 560);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7584,
                column: "OperatorId",
                value: 584);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7601,
                column: "OperatorId",
                value: 601);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7603,
                column: "OperatorId",
                value: 603);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7614,
                column: "OperatorId",
                value: 614);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7757,
                column: "OperatorId",
                value: 757);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7758,
                column: "OperatorId",
                value: 758);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7817,
                column: "OperatorId",
                value: 817);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7820,
                column: "OperatorId",
                value: 720);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 7950,
                column: "OperatorId",
                value: 950);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8330,
                column: "OperatorId",
                value: 330);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8332,
                column: "OperatorId",
                value: 332);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8398,
                column: "OperatorId",
                value: 398);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8471,
                column: "OperatorId",
                value: 871);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8533,
                column: "OperatorId",
                value: 11);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8567,
                column: "OperatorId",
                value: 867);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8719,
                column: "OperatorId",
                value: 719);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8756,
                column: "OperatorId",
                value: 875);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8818,
                column: "OperatorId",
                value: 818);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8837,
                column: "OperatorId",
                value: 837);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8838,
                column: "OperatorId",
                value: 838);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8858,
                column: "OperatorId",
                value: 858);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 8895,
                column: "OperatorId",
                value: 895);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 9112,
                column: "OperatorId",
                value: 112);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 15890,
                column: "OperatorId",
                value: 40);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 18511,
                column: "OperatorId",
                value: 15);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 19887,
                column: "OperatorId",
                value: 10);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 25666,
                column: "OperatorId",
                value: 13);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 51136,
                column: "OperatorId",
                value: 136);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 53902,
                column: "OperatorId",
                value: 93);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 55298,
                column: "OperatorId",
                value: 884);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 56995,
                column: "OperatorId",
                value: 996);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 57304,
                column: "OperatorId",
                value: 91);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 63118,
                column: "OperatorId",
                value: 38);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 66770,
                column: "OperatorId",
                value: 690);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 67040,
                column: "OperatorId",
                value: 688);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 67784,
                column: "OperatorId",
                value: 87);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 68020,
                column: "OperatorId",
                value: 687);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 68659,
                column: "OperatorId",
                value: 685);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 69019,
                column: "OperatorId",
                value: 695);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 69337,
                column: "OperatorId",
                value: 83);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 70408,
                column: "OperatorId",
                value: 689);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 71242,
                column: "OperatorId",
                value: 686);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 71587,
                column: "OperatorId",
                value: 693);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 71706,
                column: "OperatorId",
                value: 691);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 71986,
                column: "OperatorId",
                value: 78);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 72451,
                column: "OperatorId",
                value: 692);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 72842,
                column: "OperatorId",
                value: 694);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 72907,
                column: "OperatorId",
                value: 75);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 74748,
                column: "OperatorId",
                value: 684);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 75680,
                column: "OperatorId",
                value: 700);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 76180,
                column: "OperatorId",
                value: 89);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 76414,
                column: "OperatorId",
                value: 88);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 77127,
                column: "OperatorId",
                value: 86);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 88412,
                column: "OperatorId",
                value: 3);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 88579,
                column: "OperatorId",
                value: 579);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 89710,
                column: "OperatorId",
                value: 710);

            migrationBuilder.UpdateData(
                table: "City",
                keyColumn: "Id",
                keyValue: 89897,
                column: "OperatorId",
                value: 897);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OperatorId",
                table: "City");
        }
    }
}
